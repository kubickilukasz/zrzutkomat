using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

/*! \mainpage Zrzutkomat
 *
 *  projekt wykrywający i zliczający banknoty w AR.
 * \section intro_sec Autorzy
 *   Filip Gaida,<br>
 *   Łukasz Kubicki,<br>
 *   Dawid Rudy,<br>
 *   Tomasz Andrzejewski,<br>
 *   Witold Drzazga
 */

/// <summary>
/// Zarządca wykrywania i obsługi wykrytych banknotów.
/// </summary>
public class CashRecognizer : MonoBehaviour{
    /// <summary>
    /// Menedżer ARFoundation do śledzenia obrazów.
    /// </summary>
    [SerializeField] ARTrackedImageManager imageManager;
    /// <summary>
    /// Menedżer ARFoundation do śledzenia przecięcia promieni z kamery rzucanymi na obiekty.
    /// </summary>
    [SerializeField] ARRaycastManager raycastManager;
    /// <summary>
    /// Obiekt, którego kopia jest tworzona w wirtualnej przestrzeni nad aktywnym banknotem.
    /// </summary>
    [SerializeField] CashReference prefab;
    /// <summary>
    /// Definicje banknotów i ich wartooci.
    /// </summary>
    [SerializeField] List<Cash> cashDefinitions = new List<Cash> ();
    /// <summary>
    /// Dystans poniżej którego banknot jest uwawany za ten sam banknot co wcześniej wykryty.
    /// </summary>
    [SerializeField] public float maxDistance = 0.05f;
    /// <summary>
    /// Śledzone obrazy w fizycznej przestrzeni.
    /// </summary>
    List<ARTrackedImage> trackedImages = new List<ARTrackedImage>();
    /// <summary>
    /// Pula stworzonych obiektów.
    /// </summary>
    List<CashReference> spawnedPrefabs = new List<CashReference>();
    /// <summary>
    /// Pula obiektów uwzględnionych w kalkulacji.
    /// </summary>
    List<CashReference> toCalculate = new List<CashReference>();
    /// <summary>
    /// Trafienia promieni z kamery rzucanymi na obiekty.
    /// </summary>
    List<ARRaycastHit> hits = new List<ARRaycastHit>();

    /// <summary>
    /// Aktualna suma banknotów w złotych polskich.
    /// </summary>
    float currentCash = 0;
    /// <summary>
    /// Informacja czy śledzenie odbywa się w sposób nieograniczony. 
    /// Przyjmuje wartość False w przypadku zasłaniania kamery lub zbyt niewyraźnego obrazu.
    /// </summary>
    bool isGoodTracking;

    /// <summary>
    /// Kamera urządzenia.
    /// </summary>
    Camera arCam;
    /// <summary>
    /// Ostatnie trafienie promienim w obiekt.
    /// </summary>
    RaycastHit _hit;
    /// <summary>
    /// Informacja czy wystąpiło trafienie w obiekt.
    /// </summary>
    bool wasHit = false;
    /// <summary>
    /// Ostatni dystans do trafionego obiektu z tym samym obrazem.
    /// </summary>
    float lastDistance;

    /// <summary>
    /// Aktualna suma pieniędzy
    /// </summary>
    public float CurrentCash => currentCash;
    /// <summary>
    /// Liczba aktualnie śledzonych obrazów.
    /// </summary>
    public int CountTrackedImages => trackedImages.Count;
    /// <summary>
    /// Liczba aktualnie stworzonych obiektów dla banknotów.
    /// </summary>
    public int CountSpawnedPrefabs => spawnedPrefabs.Count;
    /// <summary>
    /// Ilość trafień. Przekształca wasHit na wartość z zbioru {1, 0}
    /// </summary>
    public int CountHits => wasHit ? 1 : 0;
    /// <summary>
    /// Dodatkowe informacje debugowania.
    /// </summary>
    public string AdditionalInfo => string.Join(" ", spawnedPrefabs.Select(x => x.trackedImage != null ? x.trackedImage.referenceImage.texture.name : "null").ToArray());

    /// <summary>
    /// Inicjalizacja obsługi zdarzeń ARFoundation.
    /// </summary>
    void Start(){
        arCam = GameObject.Find("AR Camera").GetComponent<Camera>();
        imageManager.trackedImagesChanged += OnChangeTrackedImage;
        ARSession.stateChanged += StateChanged;
    }

    /// <summary>
    /// Aktualizacja obsługi kliknięć na ekranie, w celu tworzenia rzutów promieni.
    /// </summary>
    void Update() {
        if (Input.touchCount == 0) {
            return;
        }

        Touch touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Began/* && raycastManager.Raycast(touch.position, hits)*/) {
            wasHit = false;
            Ray ray = arCam.ScreenPointToRay(touch.position);
            if (Physics.Raycast(ray, out _hit) && _hit.collider.TryGetComponent(out CashReference cashReference)) {
                wasHit = true;
                cashReference.OnPointerClick(null);
            }
        }
    }

    /// <summary>
    /// Zakończenie działania. Usunięcie obsługi zdarzeń ARFoundation.
    /// </summary>
    void OnDestroy() {
        imageManager.trackedImagesChanged -= OnChangeTrackedImage;
        ARSession.stateChanged -= StateChanged;
    }

    /// <summary>
    /// Uśredniona pozycja banknotów.
    /// </summary>
    /// <returns>Pozycja w świecie.</returns>
    public Vector3 GetMeanPositionCash() {
        Vector3 vector3 = new Vector3();
        int n = 0;
        spawnedPrefabs.ForEach(x => { if (x.IsSelected) { vector3 += x.transform.position; n++; } });
        if (n > 0) {
            vector3 /= n;
        }
        return vector3;
    }

    /// <summary>
    /// Uwzględnienie banknotu w sumie.
    /// </summary>
    /// <param name="cashReference">Obiekt przypisany do banknotu.</param>
    public void SetToCalculate(CashReference cashReference) {
        if (!toCalculate.Contains(cashReference)) {
            toCalculate.Add(cashReference);
            currentCash += cashReference.value;
        }
    }

    /// <summary>
    /// Usunięcie obiektu z zbioru śledzonych obiektów.
    /// </summary>
    /// <param name="cashReference">Śledzony obiekt.</param>
    public void RemoveFromCalculate(CashReference cashReference) {
        if (toCalculate.Contains(cashReference)) {
            toCalculate.Remove(cashReference);
            currentCash -= cashReference.value;
        }
    }

    /// <summary>
    /// Usunięcie obiektu przypisanego do banknotu.
    /// </summary>
    /// <param name="cashReference">Obiekt przypisany do banknotu.</param>
    public void DestroyCashReference(CashReference cashReference) {
        if (spawnedPrefabs.Contains(cashReference)) {
            spawnedPrefabs.Remove(cashReference);
            Destroy(cashReference.gameObject);
        }
    }

    /// <summary>
    /// Obsługa zmiany w śledzeniu obrazów. Aktualizacja obiektów dla obrazów, które nie zostały uwzględnione w sumie.
    /// </summary>
    /// <param name="args">Listy dodanych, zmienionych i usuniętch obrazów.</param>
    void OnChangeTrackedImage(ARTrackedImagesChangedEventArgs args) {
        InvalidateAll();
        trackedImages.AddRange(args.added);
        trackedImages.AddRange(args.updated);
        SpawnPrefabs();
        CalculateCash();
    }

    /// <summary>
    /// Obsługa zmiany stanu śledzenia. Aktualizacja wartości pola isGoodTracking.
    /// </summary>
    /// <param name="args">Ignorowane argumenty. Stan jest odczytywany z statycznej klasy.</param>
    void StateChanged(ARSessionStateChangedEventArgs args) {
        isGoodTracking = ARSession.notTrackingReason != NotTrackingReason.Initializing 
            && ARSession.notTrackingReason != NotTrackingReason.CameraUnavailable
            && ARSession.notTrackingReason != NotTrackingReason.Unsupported
            && ARSession.notTrackingReason != NotTrackingReason.ExcessiveMotion
            && ARSession.notTrackingReason != NotTrackingReason.InsufficientLight;
        if (!isGoodTracking) {
            InvalidateAll();
        }
    }

    /// <summary>
    /// Procedura tworzenia obiektów dla wykrytych obrazów.
    /// </summary>
    void SpawnPrefabs() {
        currentCash = 0;
        foreach (ARTrackedImage image in trackedImages) {
            if (TryGetCash(image, out Cash cash)) {
                if (CheckIfExist(image, out List<CashReference> cashReferences)) {
                    bool wasNotSelected = false;
                    cashReferences.ForEach(x => {
                        if (!x.IsSelected && !wasNotSelected) {
                            wasNotSelected = true;
                            x.trackedImage = image;
                        } else {
                            x.trackedImage = null;
                        }
                    });
                } else {
                    CashReference go = Instantiate(prefab);
                    spawnedPrefabs.Add(go);
                    go.Set(this, image, cash.value);
                }
            }
        }
    }

    /// <summary>
    /// Sprawdzenie czy banknot został już wykryty.
    /// </summary>
    /// <param name="image">Wykryty obraz.</param>
    /// <param name="cashReferences">Utworzone obiekty dla banknotów.</param>
    /// <returns>True, jeśli banknot został wykryty.</returns>
    bool CheckIfExist(ARTrackedImage image, out List<CashReference> cashReferences) {
        cashReferences = new List<CashReference>();
        List<CashReference> sameImages = spawnedPrefabs.FindAll(x => x.referenceTexture == image.referenceImage.texture);
        foreach (CashReference i in sameImages) {
            if (i != null) {
                lastDistance = Vector3.Distance(i.transform.position, image.transform.position);
                if (lastDistance <= maxDistance) {
                    cashReferences.Add(i);
                }
            }
        }
        return cashReferences.Count > 0;
    }

    /// <summary>
    /// Usunięcie obrazów nieuwzględnionych w sumie.
    /// </summary>
    void InvalidateAll() {
        trackedImages = new List<ARTrackedImage>();
        currentCash = 0;
    }

    /// <summary>
    /// Obliczenie aktualnej sumy banknotów.
    /// </summary>
    void CalculateCash() {
        currentCash = 0;
        foreach (CashReference cash in toCalculate) {
            currentCash += cash.value;
        }
    }

    /// <summary>
    /// Próba uzyskania definicji banknotu dla śledzonego obrazu.
    /// Dopasowanie odbywa się na podstawie przypisanej tekstury.
    /// </summary>
    /// <param name="image">Śledzony obraz</param>
    /// <param name="cash">Definicja banknotu</param>
    /// <returns>Definicja banknotu. Null w przypadku braku możliwości dopasowania jakiejkolwiek definicji.</returns>
    bool TryGetCash(ARTrackedImage image, out Cash cash) {
        cash = cashDefinitions.Find(x => x.texture == image.referenceImage.texture);
        return cash != null;
    }

    /// <summary>
    /// Definicja banknotu.
    /// </summary>
    [System.Serializable]
    public class Cash {
        /// <summary>
        /// Tekstura banknotu. 
        /// Domyślnie jest to skan banknotu z jednej ze stron.
        /// </summary>
        public Texture texture;
        /// <summary>
        /// Wartość banknotu w złotych polskich.
        /// </summary>
        public float value;
    }

}
