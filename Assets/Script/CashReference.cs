using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

/// <summary>
/// Obiekt tworzony w wirtualnej przestrzeni nad aktywnym banknotem. 
/// Zarządza rozmiarem i wyglądem rysowanego obiektu oraz jego uwzględnieniem
/// w procesie obliczania sumy wartości wykrytych banknotów.
/// </summary>
public class CashReference : MonoBehaviour, IPointerClickHandler {

    /// <summary>
    /// Tekstura dla obiektów przypisanych do banknotów uwzględnianych w sumie.
    /// </summary>
    [SerializeField] Texture inCalculations;
    /// <summary>
    /// Tekstura dla obiektów przypisanych do banknotów uwzględnianych w sumie.
    /// </summary>
    [SerializeField] Texture notCalculate;
    /// <summary>
    /// Komponent renderujący obiekt.
    /// </summary>
    [SerializeField] Renderer renderer;

    /// <summary>
    /// Informacja czy przypisany banknot jest uwzględniony w sumie.
    /// </summary>
    public bool IsSelected { private set; get; } = false;

    /// <summary>
    /// Tekstura banknotu.
    /// </summary>
    public Texture2D referenceTexture { private set; get; }
    /// <summary>
    /// Wartość banknotu w złotych polskich.
    /// </summary>
    public float value;
    /// <summary>
    /// Obiekt za którym podąża obiekt przypisany do banknotu.
    /// </summary>
    GameObject gameObjectReference;

    /// <summary>
    /// Zarządca wykrywania i obsługi wykrytych banknotów.
    /// </summary>
    CashRecognizer cashRecognizer;

    /// <summary>
    /// Śledzony obraz w przestrzeni fizycznej.
    /// </summary>
    ARTrackedImage __tImage;
    /// <summary>
    /// Śledzony obraz w przestrzeni fizycznej. Obsługuje utworzenie śledzonego obiektu, w przypadku jego braku.
    /// </summary>
    public ARTrackedImage trackedImage { 
        set {
            __tImage = value;
            if(gameObjectReference == null) gameObjectReference = new GameObject("Reference");
            if (value != null) {
                gameObjectReference.transform.SetParent(value.transform, false);
                gameObjectReference.transform.localPosition = Vector3.zero;
                gameObjectReference.transform.localRotation = Quaternion.identity;
                gameObjectReference.transform.localScale = new Vector3(value.size.x, transform.localScale.y, value.size.y);
            }
        } get {
            return __tImage;
        } 
    }

    /// <summary>
    /// Aktualizacja w każdej klatce. Aktualizowana jest aktywność, pozycja, rotacja i skala do wykrytego obrazu.
    /// </summary>
    void LateUpdate() {
        if (trackedImage == null || gameObjectReference == null || trackedImage.trackingState != TrackingState.Tracking) {
            if (!IsSelected && cashRecognizer != null) {
                cashRecognizer.DestroyCashReference(this);
            }
            return;
        }

       transform.position = gameObjectReference.transform.position;
       transform.rotation = gameObjectReference.transform.rotation;
       transform.localScale = gameObjectReference.transform.lossyScale;
    }

    /// <summary>
    /// Deinicjalizacja. Usunięcie z zbioru aktywnych banknotów.
    /// </summary>
    void OnDestroy() {
        if (cashRecognizer != null) {
            cashRecognizer.RemoveFromCalculate(this);
        }
    }

    /// <summary>
    /// Ustawienie wykrytego obrazu i przypisanego do niego banknotu.
    /// </summary>
    /// <param name="_cashRecognizer">Zarządca wykrywania i obsługi wykrytych banknotów.</param>
    /// <param name="_trackedImage">Śledzony obraz w przestrzeni fizycznej.</param>
    /// <param name="_value">Wartość banknotu w złotych polskich.</param>
    public void Set(CashRecognizer _cashRecognizer, ARTrackedImage _trackedImage, float _value) {
        cashRecognizer = _cashRecognizer;
        trackedImage = _trackedImage;
        value = _value;
        referenceTexture = _trackedImage.referenceImage.texture;
    }

    /// <summary>
    /// Obsługa wybrania obiektu na ekranie. 
    /// Kliknięcie wywołuje zmianę stanu obiektu i aktualizację przypisanej tekstury.
    /// </summary>
    /// <param name="pointerEventData">Informacje o zdarzeniu kliknięcia</param>
    public void OnPointerClick(PointerEventData pointerEventData) {
        if (IsSelected) {
            IsSelected = false;
            renderer.material.mainTexture = notCalculate;
            cashRecognizer.RemoveFromCalculate(this);
        } else {
            trackedImage = null;
            IsSelected = true;
            renderer.material.mainTexture = inCalculations;
            cashRecognizer.SetToCalculate(this);
        }
    }

}
