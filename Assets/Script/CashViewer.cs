using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Widok sumy wykrytych banknotów.
/// </summary>
public class CashViewer : MonoBehaviour{

    /// <summary>
    /// Zarządca wykrywania i obsługi wykrytych banknotów.
    /// </summary>
    [SerializeField] CashRecognizer cashRecognizer;
    /// <summary>
    /// Komponent renderujący tekst
    /// </summary>
    [SerializeField] TextMesh textMesh;
    /// <summary>
    /// Obiekt do którego jest zwrócony tekst. Domyślnie jest to pozycja kamery.
    /// </summary>
    [SerializeField] Transform targetLookAt;

    /// <summary>
    /// Aktualizacja w każdej klatce. 
    /// Aktualizowany jest wyświetlany tekst, jego pozycja oraz rotacja.
    /// Pozycja jest średnią pozycją banknotów.
    /// </summary>
    void Update() {
        textMesh.text = cashRecognizer.CurrentCash > float.Epsilon ? cashRecognizer.CurrentCash + "zł" : "";
        transform.position = cashRecognizer.GetMeanPositionCash();
        transform.LookAt(targetLookAt, Vector3.up);
    }

}
