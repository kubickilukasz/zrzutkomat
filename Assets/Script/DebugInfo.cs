using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/// <summary>
/// Klasa wspierająca debugowanie.
/// </summary>
public class DebugInfo : MonoBehaviour{

    /// <summary>
    /// Zarządca wykrywania banknotów.
    /// </summary>
    [SerializeField] CashRecognizer cashRecognizer;
    /// <summary>
    /// Komponent wyświetlający tekst.
    /// </summary>
    [SerializeField] TextMeshProUGUI text;

    /// <summary>
    /// Obsługa restartu obiektu. Pobranie komponentu wyświetlającego tekst.
    /// </summary>
    void Reset()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

#if DEVELOPMENT_BUILD

    /// <summary>
    /// Aktualizacja tekstu w każdej klatce.
    /// </summary>
    void Update(){
        text.text = 
            "Tracked image: " + cashRecognizer.CountTrackedImages + " \n" +
            "Spawned Prefabs: " + cashRecognizer.CountSpawnedPrefabs + " \n" +
            "Hits: " + cashRecognizer.CountHits + " \n" +
            "Last distance: " + cashRecognizer.LastDistance + " \n" +
            "Info: " + cashRecognizer.AdditionalInfo + " \n" +
            "Current cash: " + cashRecognizer.CurrentCash;
    }
#endif
}
